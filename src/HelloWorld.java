import com.test.*;

public class HelloWorld {

	
	public int toBeCalled(int a , int b){
		
		int c = 0;
		
			c = a+b;
		
		return c;
	}
	
	void callingMethod(){
		
		int a = 2;
		int b = 3;
		int result = 0;
		
		
		HelloWorld hello = new HelloWorld();
		
		result = hello.toBeCalled(a, b);
		
		System.out.print(result);
	}
	
	public static void main(String args[]){
	
		Dog d = new Dog();
		
		d.childClass();
		d.animalMethod();
		
		System.out.println("Hello Boss - updated");
		
	}
	
}
