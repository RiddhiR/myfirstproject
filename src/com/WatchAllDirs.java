package com.java;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;


public class WatchAllDirs {
   private static Map<WatchKey, Path> keyPathMap = new HashMap<>();

   public static void main (String[] args) throws Exception {
       try (WatchService watchService = FileSystems.getDefault().newWatchService()) {
           registerDir(Paths.get("F:\\work\\eclipse_workspace"), watchService);
           //startListening(watchService);
       }
   }

   private static void registerDir (Path path, WatchService watchService) throws
                       IOException {


       if (!Files.isDirectory(path, LinkOption.NOFOLLOW_LINKS)) {
           return;
       }

       System.out.println("Listening on dirs: " + path);


       WatchKey key = path.register(watchService,
                           StandardWatchEventKinds.ENTRY_CREATE,
                           StandardWatchEventKinds.ENTRY_MODIFY,
                           StandardWatchEventKinds.ENTRY_DELETE);
       keyPathMap.put(key, path);


       for (File f : path.toFile().listFiles()) {
           registerDir(f.toPath(), watchService);
       }
   }

   private static void startListening (WatchService watchService) throws Exception {
       while (true) {
           WatchKey queuedKey = watchService.take();
           for (WatchEvent<?> watchEvent : queuedKey.pollEvents()) {
        	   
//        	   Path pathDir = (Path) watchEvent.context();
//               Path parentPath1 = keyPathMap.get(queuedKey);
//               pathDir = parentPath1.resolve(pathDir);
//        	   
//               System.out.println("Event :: " + watchEvent.kind() + " :: File Name :: " +watchEvent.context() + " :: Directory :: "+pathDir);
               
               if (watchEvent.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                   
            	   Path path = (Path) watchEvent.context();
                   Path parentPath = keyPathMap.get(queuedKey);
                   path = parentPath.resolve(path);
                   
                   System.out.println("Event :: " + watchEvent.kind() + " :: File Name :: " +watchEvent.context() + " :: Directory :: "+path);
                   
                   registerDir(path, watchService);
               }
               
               if (watchEvent.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
                   
            	   Path path = (Path) watchEvent.context();
                   Path parentPath = keyPathMap.get(queuedKey);
                   path = parentPath.resolve(path);
                   
                   System.out.println("Event :: " + watchEvent.kind() + " :: File Name :: " +watchEvent.context() + " :: Directory :: "+path);
                   
                   
               }
               
               if (watchEvent.kind() == StandardWatchEventKinds.ENTRY_DELETE) {
                   
            	   Path path = (Path) watchEvent.context();
                   Path parentPath = keyPathMap.get(queuedKey);
                   path = parentPath.resolve(path);
                   
                   System.out.println("Event :: " + watchEvent.kind() + " :: File Name :: " +watchEvent.context() + " :: Directory :: "+path);
                   
                   
               }
           }
           if(!queuedKey.reset()){
               keyPathMap.remove(queuedKey);
           }
           if(keyPathMap.isEmpty()){
               break;
           }
       }
   }
}