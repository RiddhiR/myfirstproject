package com.java;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.TextProgressMonitor;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public class CloneGit {
	
	private static Git git;
	private static File file;
	
	
	public static void main(String[] args) throws MissingObjectException, IncorrectObjectTypeException, IOException {

		   try {
			 // cloning a repo starts
			   
			  /* CredentialsProvider cp = new UsernamePasswordCredentialsProvider("RiddhiR", "RidsGIT@2020");
			   Git.cloneRepository()
			  .setURI("https://gitlab.com/RiddhiR/myfirstproject.git")
			  .setDirectory(new File("F:\\work\\eclipse_workspace\\gitProjects")).setCredentialsProvider(cp)
			  .call();
			   
			// cloning a repo ends
			
			// changes committed in a local directory - details - starts
			   
			Iterable<RevCommit> logs;
			try {
				logs = Git.open(new File("F:\\work\\eclipse_workspace\\gitProjects")).log().call();
				for( RevCommit revCommit : logs ) {
				      System.out.println(revCommit.getAuthorIdent().getName());
				      System.out.println(revCommit.getAuthorIdent().getEmailAddress());
				      System.out.println(revCommit.getAuthorIdent().getWhen());
				    }
			} catch (IOException e) {
				e.printStackTrace();
			}*/
			
			// changes committed in a local directory - details - ends
			
			TextProgressMonitor consoleProgressMonitor = new TextProgressMonitor(new PrintWriter(System.out));
			System.out.println("\n>>> Printing status of local repository\n");
			Status status = Git.open(new File("F:\\work\\eclipse_workspace\\gitProjects")).status().setProgressMonitor(consoleProgressMonitor).call();
			
			System.out.println("Modified file = " + status.getAdded());
			
			
			
		} catch (InvalidRemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransportException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (GitAPIException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
